<img width="150px" src="https://user-images.githubusercontent.com/79089703/228573457-ee265da7-3769-4c42-be53-d8e9180dacb0.png" />

# TeBo - Culegerea Interactivă Pentru Copii

![GitHub stars](https://img.shields.io/gitlab/stars/wowvain-dev/tebo?color=fa6470)
![GitHub issues](https://img.shields.io/gitlab/issues/open/wowvain-dev/tebo?color=d8b22d)
![GitHub license](https://img.shields.io/gitlab/license/wowvain-dev/tebo)
[![Required Node.JS >= 14.18.0 || >=16.0.0](https://img.shields.io/static/v1?label=node&message=14.18.0%20||%20%3E=16.0.0&logo=node.js&color=3f893e)](https://nodejs.org/about/releases)

## 👀 Prezentare Generală

📦 Instalare simplă

🎯 Sistem de progresie uşor de urmărit    

🖥 Uşor de rulat cross-platform Windows / macOS / Linux.

💪 Construită cu 

[![My Skills](https://skillicons.dev/icons?i=ts,react,nodejs,electron,scss)](https://skillicons.dev)  

## 📄 Documentație

#### Puteți găsi o prezentare succintă a ideii din spatele aplicației la: [Google Drive](https://drive.google.com/file/d/1KgF5iCfRQPy91G2mBuxGIG3Oxivo-lXR/view?usp=sharing)
#### Puteți găsi documentația oficială a aplicației la: [Google Docs](https://docs.google.com/document/d/1-UaW0o4H4E0m2j9YGnsPfSpCxP_2zXHngUtTnCYW4OQ/edit?usp=sharing)

## 🛫 Instalare

- #### Folosirea installer-ului:
  - Intrati pe pagina de [releases](https://gitlab.com/wowvain-dev/TeBo/-/releases) si descarcati ultima versiune: ([v2.2.0-stable](https://gitlab.com/wowvain-dev/TeBo/-/releases/stable))
  - Dezarhivati fisierul descarcat
  - Rulati `tebo-<VERSIUNE>.exe` si urmariti pasii installer-ului
  - Porniti programul din windows search bar

### SAU

- #### Rulare din codul sursa:

```sh
git clone  https://gitlab.com/wowvain-dev/TeBo.git
npm install
npm run dev
```

## 📂 Aplicaţia

### KNOWN BUG: DACA PRIMESTI UN ECRAN ALB GOL UNEORI, TREBUIE SA APESI CTRL + R PENTRU A DA REFRESH LA PAGINA.
Scopul aplicaţiei este de a facilita învăţarea copiilor prin a le oferi exerciţii uşor de înţeles şi generate aleatoriu.

#### Screenshots
![image](https://gitlab.com/wowvain-dev/TeBo/-/raw/master/screenshots/main_menu.png)
![image](https://gitlab.com/wowvain-dev/TeBo/-/raw/master/screenshots/fractions.png)
![image](https://gitlab.com/wowvain-dev/TeBo/-/raw/master/screenshots/settings_menu.png)



## 🚨 Tehnologii Folosite
- ### [![My Skills](https://skillicons.dev/icons?i=nodejs)](https://skillicons.dev) NodeJS  
Baza aplicaţiei este făcută folosind Node.js.

- ### [![My Skills](https://skillicons.dev/icons?i=react)](https://skillicons.dev) React
React a fost principalul framework de front-end folosit în realizarea aplicaţiei.

- ### [![My Skills](https://skillicons.dev/icons?i=electron)](https://skillicons.dev) Electron
Am folosit Electron pentru a incapsula un client de Chromium intr-o fereastra de program care da un feel nativ aplicaţiei.

- ### [![My Skills](https://skillicons.dev/icons?i=ts)](https://skillicons.dev) TypeScript
Majoritatea aplicaţiei este scrisă în TypeScript (*.ts / *.tsx) pentru a putea să ne folosim de convenienta sistemului de type safety.

- ### [![My Skills](https://skillicons.dev/icons?i=scss)](https://skillicons.dev) Syntetically Awesome Stylesheets (Sass)
Pentru fisierele de stil ale aplicaţiei am folosit SASS/SCSS.

## 🫂 Echipa Nostra

- Profesor Coordonator: Doamna Profesoară Corina Vinţ
- Membrii:
  - Stanciu Bogdan, Lead Developer
  - Ghinescu Teo, Lead Designer


## ❔ Contact

- E-Mail: wowvain.dev@gmail.com
- Discord: !wowvain#3859

## Copyright Notice

Nu dețin/(em) niciuna dintre pozele de fundal ale aplicației.

Nu dețin/(em) iconițele folosite în aplicație. (fac parte din diverse librării puse împreuna în cadrul librăriei [react-icons](https://react-icons.github.io/react-icons/).

Nu dețin/(em) componentele din librăriile [***Ant Design***](https://ant.design), [***NextUI***](https://nextui.org), [***Tldraw***](https://tldraw.dev).

Nu dețin/(em) paragrafele de text din secțiunea de ***Știați Că*** a meniului principal.


Deținem toate variantele avatarului ***Tebo***.

Deținem fișierele audio folosite în cadrul exerciților de română.

## NOTE: Aplicația a trecut printr-un proces de re-branding. Inițial a fost numită `LiMa` și era hosted pe github.